-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  lun. 11 nov. 2019 à 12:17
-- Version du serveur :  5.7.19
-- Version de PHP :  7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `amandwza_tpa`
--

-- --------------------------------------------------------

--
-- Structure de la table `app_users`
--

DROP TABLE IF EXISTS `app_users`;
CREATE TABLE IF NOT EXISTS `app_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_C2502824F85E0677` (`username`),
  UNIQUE KEY `UNIQ_C2502824E7927C74` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `calendrier_production`
--

DROP TABLE IF EXISTS `calendrier_production`;
CREATE TABLE IF NOT EXISTS `calendrier_production` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producteur_id` int(11) DEFAULT NULL,
  `speculation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `surfaceCultive` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_debut` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_fin` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `destination` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_C8483468AB9BB300` (`producteur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `calendrier_production`
--

INSERT INTO `calendrier_production` (`id`, `producteur_id`, `speculation`, `surfaceCultive`, `date_debut`, `date_fin`, `destination`) VALUES
(1, 1, NULL, NULL, '2019-11-23', '2019-11-15', 'France');

-- --------------------------------------------------------

--
-- Structure de la table `campagne`
--

DROP TABLE IF EXISTS `campagne`;
CREATE TABLE IF NOT EXISTS `campagne` (
  `identifiant` int(11) NOT NULL AUTO_INCREMENT,
  `zone_production` int(11) DEFAULT NULL,
  `campagne` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `variete` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `n_ordre` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `code_parcelle` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `poids_brut` double NOT NULL,
  `nom_chauffeur` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `heure_depart` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `heure_arrivee` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `immatriculation_camion` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `nom_responsable_parcelle` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_cageots` int(11) NOT NULL,
  `poids_net` double NOT NULL,
  `date_reception` date NOT NULL,
  PRIMARY KEY (`identifiant`),
  KEY `FK_F3E43E25AB9BB300` (`zone_production`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `campagne`
--

INSERT INTO `campagne` (`identifiant`, `zone_production`, `campagne`, `variete`, `n_ordre`, `code_parcelle`, `poids_brut`, `nom_chauffeur`, `heure_depart`, `heure_arrivee`, `immatriculation_camion`, `nom_responsable_parcelle`, `nombre_cageots`, `poids_net`, `date_reception`) VALUES
(1, 1, 'Mangue', 'mangue', '1', '1', 123, 'diop', '8h', '11h', 'BFGZFR', 'diop', 12, 123, '2019-11-11');

-- --------------------------------------------------------

--
-- Structure de la table `certificat`
--

DROP TABLE IF EXISTS `certificat`;
CREATE TABLE IF NOT EXISTS `certificat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producteur_id` int(11) DEFAULT NULL,
  `numero_cert` int(11) DEFAULT NULL,
  `date_cert` date DEFAULT NULL,
  `date_exp` date DEFAULT NULL,
  `certificat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_27448F77AB9BB300` (`producteur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `certificat`
--

INSERT INTO `certificat` (`id`, `producteur_id`, `numero_cert`, `date_cert`, `date_exp`, `certificat`) VALUES
(1, 1, 1, '2019-11-15', '2019-11-14', 'GLOBALGAP Option 1'),
(2, 1, 2, '2019-11-14', '2019-11-28', 'GLOBALGAP Option 2'),
(3, 1, 3, '2019-11-14', '2019-11-21', 'Tesco Nature\'s Choice'),
(4, 1, 4, '2019-11-15', '2019-11-28', 'Agriculture Biologique'),
(5, 1, 5, '2019-11-15', '2019-11-13', 'BRC'),
(6, 1, 6, '2019-11-21', '2019-11-22', 'Autres (à préciser)');

-- --------------------------------------------------------

--
-- Structure de la table `demande`
--

DROP TABLE IF EXISTS `demande`;
CREATE TABLE IF NOT EXISTS `demande` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producteur_id` int(11) DEFAULT NULL,
  `typeDemande` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateDepot` datetime DEFAULT NULL,
  `statut` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `producteur_id` (`producteur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `demande`
--

INSERT INTO `demande` (`id`, `producteur_id`, `typeDemande`, `dateDepot`, `statut`) VALUES
(1, 1, 'Nouvelle', '2019-11-11 09:44:53', 1);

-- --------------------------------------------------------

--
-- Structure de la table `demande_inspection`
--

DROP TABLE IF EXISTS `demande_inspection`;
CREATE TABLE IF NOT EXISTS `demande_inspection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom_prenom_demandeur` varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statuts` int(11) NOT NULL,
  `nom_entreprise` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numero_telephone_demandeur` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_appel` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zone_provenance` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proprietaire_verger` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proprietaire_verger1` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `proprietaire_verger2` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieu_conditionnement` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_conditionnement` varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `demande_inspection`
--

INSERT INTO `demande_inspection` (`id`, `nom_prenom_demandeur`, `statuts`, `nom_entreprise`, `numero_telephone_demandeur`, `date_appel`, `zone_provenance`, `proprietaire_verger`, `proprietaire_verger1`, `proprietaire_verger2`, `lieu_conditionnement`, `date_conditionnement`) VALUES
(1, 'abdoulaye diallo', 0, 'miname', '12736454', '2019-10-29', 'joal', 'gueye', 'gueye', 'ndiaye', 'joal', '2019-11-13');

-- --------------------------------------------------------

--
-- Structure de la table `deposerdemande`
--

DROP TABLE IF EXISTS `deposerdemande`;
CREATE TABLE IF NOT EXISTS `deposerdemande` (
  `demande_id` int(11) NOT NULL,
  `producteur_id` int(11) NOT NULL,
  PRIMARY KEY (`demande_id`,`producteur_id`),
  KEY `IDX_21CAAE6880E95E18` (`demande_id`),
  KEY `IDX_21CAAE68AB9BB300` (`producteur_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `detail_lot`
--

DROP TABLE IF EXISTS `detail_lot`;
CREATE TABLE IF NOT EXISTS `detail_lot` (
  `identifiant` int(11) NOT NULL AUTO_INCREMENT,
  `id_campagne` int(11) DEFAULT NULL,
  `id_lot` int(11) DEFAULT NULL,
  `calibre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_carton` int(11) NOT NULL,
  `poids_net` double NOT NULL,
  `n_palette` int(11) NOT NULL,
  PRIMARY KEY (`identifiant`),
  KEY `FK5qbslk1ioudummgell60ha644478` (`id_lot`),
  KEY `FK5qbslk1ioudummg` (`id_campagne`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `exportation`
--

DROP TABLE IF EXISTS `exportation`;
CREATE TABLE IF NOT EXISTS `exportation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `denomEntreprise` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenoNomInspecteur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateInspection` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lieuInspection` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `personneRencontre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valide` int(11) NOT NULL,
  `fonctionpersonneRencontre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `variete` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `categorie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qteDeclare` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nbColis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typeTransport` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tailleEchantillon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nbColiControle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nbMangueControle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `registreReceptionMangue` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `declarationSupp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marqueColis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conformiteColis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numeroLot` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conformiteLot` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numeroPalette` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conformitePalette` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `maturite` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `piqureMoucheFruit` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pourriturePendonculaire` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `charanconNoyaux` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `traceArthrocnose` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marquageNIMP15` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `etatSanitaireGeneral` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentaire` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fiche_exportation`
--

DROP TABLE IF EXISTS `fiche_exportation`;
CREATE TABLE IF NOT EXISTS `fiche_exportation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_producteur` int(11) DEFAULT NULL,
  `id_palettisation` int(11) DEFAULT NULL,
  `id_campage` int(11) DEFAULT NULL,
  `numero_exporation` int(11) NOT NULL,
  `date_exportation` datetime NOT NULL,
  `moyen_utilise` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `pays_exportation` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nom_compagnie` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `nom_recepteur` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commentaire` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_reception` datetime DEFAULT NULL,
  `statut` int(11) NOT NULL,
  `variete` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_F3E43E25A255zza` (`id_producteur`),
  KEY `FK_F3E43E25A2bff47` (`id_campage`),
  KEY `FK_F3E43E25A25dd58d4` (`id_palettisation`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `fiche_exportation`
--

INSERT INTO `fiche_exportation` (`id`, `id_producteur`, `id_palettisation`, `id_campage`, `numero_exporation`, `date_exportation`, `moyen_utilise`, `pays_exportation`, `nom_compagnie`, `nom_recepteur`, `commentaire`, `date_reception`, `statut`, `variete`, `photo`) VALUES
(1, 1, 1, 1, 1, '2019-11-11 10:40:37', 'Vol', 'Autres', 'minam SARl', NULL, NULL, NULL, 0, 'Mangue', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `fiche_operateur`
--

DROP TABLE IF EXISTS `fiche_operateur`;
CREATE TABLE IF NOT EXISTS `fiche_operateur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `conditionnement_id` int(11) DEFAULT NULL,
  `tableTravail` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `chambreFroide` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `temperature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conformite` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emballage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sanitaire` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `securite` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `pharmacie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lavage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `installation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marcheEnAvant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qualiteCond` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `docArchive` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `MaterielAutoControle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `responsableQualite` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `niveauResponsable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `marquageEmballage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uniteTraitementNTMP` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paletteEmbTraite` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `paletteEmbRecup` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supportEnreg` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `supportAJour` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteChambre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteSysteme` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteConformiteTemp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteUniteTrait` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteSanitaire` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `afficheSecurite` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notePharmacie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notelavageMain` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteConformiteInstallation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteMarcheAvant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteQualiteCond` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteDocArchivage` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteMaterielAutoControle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteResponsableQualite` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteNiveauResponsable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `typeCertificat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteCertificat` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tesco` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bio` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `globalGap` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_33D94475A222637` (`conditionnement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

DROP TABLE IF EXISTS `fos_user`;
CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, 'adminuser', 'adminuser', 'mlouma@gmail.com', 'mlouma@gmail.com', 1, NULL, '$2y$13$i50sAlID6JwEJEpeh.yjyuYDM/WAi/NkhthGGk3qM/mjd4U2FmvPO', '2019-10-08 20:23:18', NULL, NULL, 'a:3:{i:0;s:11:\"ROLE_GERANT\";i:1;s:16:\"ROLE_SUPER_ADMIN\";i:2;s:10:\"ROLE_ADMIN\";}'),
(3, 'admin', 'admin', 'admin@admin.com', 'admin@admin.com', 1, NULL, '$2y$13$6r9SY3If01TLxFrqGgDtyO9BdMKwgBeOqNCHDVGRdfZ2990KfpwYS', '2019-10-28 12:34:59', NULL, NULL, 'a:1:{i:0;s:11:\"ROLE_GERANT\";}'),
(6, 'adminusers', 'adminusers', 'adminusers@adminusers', 'adminusers@adminusers', 1, NULL, '$2y$13$sarsfXD82EP8bSWSACMFSOkSj90/jj6LcJJ7FVGXnft8Fz3a3gKwa', '2019-11-11 09:45:10', NULL, NULL, 'a:2:{i:0;s:11:\"ROLE_GERANT\";i:1;s:16:\"ROLE_SUPER_ADMIN\";}'),
(25, 'nanndiaye', 'nanndiaye', 'nanndiaye68@gmail.com', 'nanndiaye68@gmail.com', 1, NULL, '$2y$13$IpDzrOLB8d4ZhbyQ099aCudHUlqYj0BnOLaGmt5YyjR8JeeUywEVC', NULL, NULL, NULL, 'a:1:{i:0;s:15:\"ROLE_PRODUCTEUR\";}');

-- --------------------------------------------------------

--
-- Structure de la table `gestion_qualite`
--

DROP TABLE IF EXISTS `gestion_qualite`;
CREATE TABLE IF NOT EXISTS `gestion_qualite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `systemGesQual` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gestionInterne` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `consultant` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autre` varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteAutre` varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `inspection_mangue`
--

DROP TABLE IF EXISTS `inspection_mangue`;
CREATE TABLE IF NOT EXISTS `inspection_mangue` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `referenceNotification` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exportateur` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numeroEnregistrement` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `motifNotification` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `quantite` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refCertifPhyto` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `refFichInspection` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `equipeInvestig` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conditionnement_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6AC7E186A222637` (`conditionnement_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `lot`
--

DROP TABLE IF EXISTS `lot`;
CREATE TABLE IF NOT EXISTS `lot` (
  `identifiant` int(11) NOT NULL AUTO_INCREMENT,
  `id_campagne` int(11) DEFAULT NULL,
  `code_lot` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `n_lot_recolte` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_cageots` int(11) NOT NULL,
  `poids_brut` double NOT NULL,
  `poids_net` double NOT NULL,
  PRIMARY KEY (`identifiant`),
  KEY `FK5qbslk1ioudummgell60ha65585587` (`id_campagne`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `lot`
--

INSERT INTO `lot` (`identifiant`, `id_campagne`, `code_lot`, `n_lot_recolte`, `nombre_cageots`, `poids_brut`, `poids_net`) VALUES
(1, NULL, '1', '1', 123, 12, 23);

-- --------------------------------------------------------

--
-- Structure de la table `personne`
--

DROP TABLE IF EXISTS `personne`;
CREATE TABLE IF NOT EXISTS `personne` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `statut` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `num_tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `num_fixe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `num_fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `personnes`
--

DROP TABLE IF EXISTS `personnes`;
CREATE TABLE IF NOT EXISTS `personnes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `num_tel` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `num_fixe` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `num_fax` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `statut` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `producteur`
--

DROP TABLE IF EXISTS `producteur`;
CREATE TABLE IF NOT EXISTS `producteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenom` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addresse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `statutLegal` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ninea` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `siegeSocial` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `region` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ville` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `departement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `commune` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `arrondissement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `village` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numTel` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `numFixe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fex` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenomRep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomRep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `addresseRep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fonctionRep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `mobileRep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fixeRep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `faxeRep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailRep` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nomManager` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenomManager` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `titreManager` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telManager` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `emailManager` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `conditionnement` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `exportation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vulgarisation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `bordChamp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `europe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `enStation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `autre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `gestionInterne` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `consultant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `noteProduction` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteExportation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `noteCond` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `notePrestation` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `producteur`
--

INSERT INTO `producteur` (`id`, `nom`, `prenom`, `addresse`, `statutLegal`, `ninea`, `siegeSocial`, `region`, `ville`, `departement`, `commune`, `arrondissement`, `village`, `numTel`, `numFixe`, `fex`, `email`, `prenomRep`, `nomRep`, `addresseRep`, `fonctionRep`, `mobileRep`, `fixeRep`, `faxeRep`, `emailRep`, `nomManager`, `prenomManager`, `titreManager`, `telManager`, `emailManager`, `conditionnement`, `exportation`, `vulgarisation`, `bordChamp`, `europe`, `regie`, `enStation`, `autre`, `gestionInterne`, `consultant`, `date`, `noteProduction`, `noteExportation`, `noteCond`, `notePrestation`) VALUES
(1, 'diop', 'astou', NULL, NULL, '1231', 'MEDINA', 'Dakar', NULL, 'Rufisque', NULL, NULL, NULL, '746464646', '645353735', NULL, 'nanndiaye68@gmail.com', 'astou', 'diop', NULL, 'producteur', '746464646', NULL, NULL, 'nanndiaye68@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1', NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, 'note', 'note', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `registre_palettisation`
--

DROP TABLE IF EXISTS `registre_palettisation`;
CREATE TABLE IF NOT EXISTS `registre_palettisation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code_lot` int(11) DEFAULT NULL,
  `id_campagne` int(11) DEFAULT NULL,
  `date` date NOT NULL,
  `n_palette` int(11) NOT NULL,
  `nbre_cartons` int(11) NOT NULL,
  `total_cartons` int(11) NOT NULL,
  `heure_entree_ch_froide` int(11) NOT NULL,
  `heure_sortie_ch_froide` int(11) NOT NULL,
  `destinataire` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `parcking_liste` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `paragraphe` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `produit_calibre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `statut` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK5qbslk1ioudummgell60ha6` (`id_campagne`),
  KEY `FK5qbslk1ioudummgell60ha6hhgfd` (`code_lot`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `registre_palettisation`
--

INSERT INTO `registre_palettisation` (`id`, `code_lot`, `id_campagne`, `date`, `n_palette`, `nbre_cartons`, `total_cartons`, `heure_entree_ch_froide`, `heure_sortie_ch_froide`, `destinataire`, `parcking_liste`, `paragraphe`, `produit_calibre`, `statut`) VALUES
(1, 1, 1, '2019-11-14', 1, 11, 10, 8, 12, 'France', 'kejjz', 'erfefer', 'calibre', 1),
(2, 1, 1, '2019-11-30', 2, 10, 10, 9, 11, 'Gambie', 're', 'fsvef', 'sdfvef', 0);

-- --------------------------------------------------------

--
-- Structure de la table `registre_reception`
--

DROP TABLE IF EXISTS `registre_reception`;
CREATE TABLE IF NOT EXISTS `registre_reception` (
  `ordre_arrive` int(11) NOT NULL AUTO_INCREMENT,
  `id_campagne` int(11) DEFAULT NULL,
  `parcelle_origine` varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  `date_recolte` date NOT NULL,
  `variete` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nombre_cageots` int(11) NOT NULL,
  `poids_brut` int(11) NOT NULL,
  `poids_net` int(11) NOT NULL,
  `code_attribue` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`ordre_arrive`),
  KEY `FK5qbslk1ioudummgell60ha69j` (`id_campagne`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `site_conditionnement`
--

DROP TABLE IF EXISTS `site_conditionnement`;
CREATE TABLE IF NOT EXISTS `site_conditionnement` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producteur_id` int(11) DEFAULT NULL,
  `speculation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `siteCond` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `zones` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contrat_prestation` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contrat_location` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lacalisation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_31D134D1AB9BB300` (`producteur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `site_conditionnement`
--

INSERT INTO `site_conditionnement` (`id`, `producteur_id`, `speculation`, `siteCond`, `zones`, `contrat_prestation`, `contrat_location`, `lacalisation`) VALUES
(1, 1, NULL, 'APAD', '1', 'non', 'non', 'Diouloulou');

-- --------------------------------------------------------

--
-- Structure de la table `site_production`
--

DROP TABLE IF EXISTS `site_production`;
CREATE TABLE IF NOT EXISTS `site_production` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `producteur_id` int(11) DEFAULT NULL,
  `perimetre` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prod_associe` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `piste_associe` varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localisation` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `superficie` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `speculation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nb_spec` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `regie` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_F3E43E25AB9BB3002558` (`producteur_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `site_production`
--

INSERT INTO `site_production` (`id`, `producteur_id`, `perimetre`, `prod_associe`, `piste_associe`, `localisation`, `superficie`, `speculation`, `nb_spec`, `regie`) VALUES
(1, 1, 'mangue', NULL, NULL, 'Diouloulou', '1000', NULL, '1', NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `calendrier_production`
--
ALTER TABLE `calendrier_production`
  ADD CONSTRAINT `FK_C8483468AB9BB300` FOREIGN KEY (`producteur_id`) REFERENCES `producteur` (`id`);

--
-- Contraintes pour la table `campagne`
--
ALTER TABLE `campagne`
  ADD CONSTRAINT `FK_539B5D16F30CA200` FOREIGN KEY (`zone_production`) REFERENCES `site_production` (`id`);

--
-- Contraintes pour la table `certificat`
--
ALTER TABLE `certificat`
  ADD CONSTRAINT `FK_27448F77AB9BB300` FOREIGN KEY (`producteur_id`) REFERENCES `producteur` (`id`);

--
-- Contraintes pour la table `demande`
--
ALTER TABLE `demande`
  ADD CONSTRAINT `FK_2694D7A5AB9BB300` FOREIGN KEY (`producteur_id`) REFERENCES `producteur` (`id`);

--
-- Contraintes pour la table `detail_lot`
--
ALTER TABLE `detail_lot`
  ADD CONSTRAINT `FK_3C3EFB8F340B183` FOREIGN KEY (`id_campagne`) REFERENCES `campagne` (`identifiant`),
  ADD CONSTRAINT `FK_3C3EFB8F9525C141` FOREIGN KEY (`id_lot`) REFERENCES `lot` (`identifiant`);

--
-- Contraintes pour la table `fiche_exportation`
--
ALTER TABLE `fiche_exportation`
  ADD CONSTRAINT `FK_D1A722F86EEB2649` FOREIGN KEY (`id_campage`) REFERENCES `campagne` (`identifiant`),
  ADD CONSTRAINT `FK_D1A722F88CF4D98B` FOREIGN KEY (`id_palettisation`) REFERENCES `registre_palettisation` (`id`),
  ADD CONSTRAINT `FK_D1A722F8DB516EC7` FOREIGN KEY (`id_producteur`) REFERENCES `producteur` (`id`);

--
-- Contraintes pour la table `fiche_operateur`
--
ALTER TABLE `fiche_operateur`
  ADD CONSTRAINT `FK_33D94475A222637` FOREIGN KEY (`conditionnement_id`) REFERENCES `site_conditionnement` (`id`);

--
-- Contraintes pour la table `lot`
--
ALTER TABLE `lot`
  ADD CONSTRAINT `FK_B81291B340B183` FOREIGN KEY (`id_campagne`) REFERENCES `campagne` (`identifiant`);

--
-- Contraintes pour la table `registre_palettisation`
--
ALTER TABLE `registre_palettisation`
  ADD CONSTRAINT `FK_A9C0C1C5340B183` FOREIGN KEY (`id_campagne`) REFERENCES `campagne` (`identifiant`),
  ADD CONSTRAINT `FK_A9C0C1C5E3A11EB1` FOREIGN KEY (`code_lot`) REFERENCES `lot` (`identifiant`);

--
-- Contraintes pour la table `registre_reception`
--
ALTER TABLE `registre_reception`
  ADD CONSTRAINT `FK_276945A3340B183` FOREIGN KEY (`id_campagne`) REFERENCES `campagne` (`identifiant`);

--
-- Contraintes pour la table `site_conditionnement`
--
ALTER TABLE `site_conditionnement`
  ADD CONSTRAINT `FK_31D134D1AB9BB300` FOREIGN KEY (`producteur_id`) REFERENCES `producteur` (`id`);

--
-- Contraintes pour la table `site_production`
--
ALTER TABLE `site_production`
  ADD CONSTRAINT `FK_F3E43E25AB9BB300` FOREIGN KEY (`producteur_id`) REFERENCES `producteur` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
