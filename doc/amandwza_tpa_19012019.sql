-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  Dim 19 jan. 2020 à 19:10
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  amandwza_tpa
--

-- --------------------------------------------------------

--
-- Structure de la table app_users
--

DROP TABLE IF EXISTS  app_users ;
CREATE TABLE IF NOT EXISTS  app_users 
(
    id int
(
    11
) NOT NULL AUTO_INCREMENT,
     username  varchar
(
    25
) COLLATE utf8_unicode_ci NOT NULL,
     password  varchar
(
    64
) COLLATE utf8_unicode_ci NOT NULL,
     email  varchar
(
    60
) COLLATE utf8_unicode_ci NOT NULL,
     is_active  tinyint
(
    1
) NOT NULL,
    PRIMARY KEY
(
    id
),
    UNIQUE KEY  UNIQ_C2502824F85E0677 
(
    username
),
    UNIQUE KEY  UNIQ_C2502824E7927C74 
(
    email
)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE =utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table calendrier_production
--

DROP TABLE IF EXISTS  calendrier_production ;
CREATE TABLE IF NOT EXISTS  calendrier_production 
(
    id int
(
    11
) NOT NULL AUTO_INCREMENT,
     producteur_id  int
(
    11
) DEFAULT NULL,
     speculation  varchar
(
    50
) COLLATE utf8_unicode_ci DEFAULT NULL,
     surfaceCultive  varchar
(
    100
) COLLATE utf8_unicode_ci DEFAULT NULL,
     date_debut  varchar
(
    255
) COLLATE utf8_unicode_ci DEFAULT NULL,
     date_fin  varchar
(
    255
) COLLATE utf8_unicode_ci DEFAULT NULL,
     destination  varchar
(
    50
) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY
(
    id
),
    KEY  IDX_C8483468AB9BB300 
(
    producteur_id
)
    ) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE =utf8_unicode_ci;

--
-- Déchargement des données de la table calendrier_production
--

INSERT INTO  calendrier_production  ( id ,  producteur_id ,  speculation ,  surfaceCultive ,  date_debut , 
                                       date_fin ,  destination )
VALUES
    (1, 4, NULL, NULL, '2020-01-10',
                       '2020-02-01',
                       'Allemagne'),
    (2, 4, NULL, NULL,
                       '2020-01-04',
                       '2020-01-11',
                       'Allemagne'),
    (3, 5, NULL, NULL,
                       '',
                       '',
                       'France'),
    (4, 5, NULL, NULL,
                       '',
                       '',
                       'France'),
    (5, 6, NULL, NULL,
                       '',
                       '',
                       'France'),
    (6, 6, NULL, NULL,
                       '',
                       '',
                       'France'),
    (7, 7, NULL, NULL,
                       '',
                       '',
                       'France'),
    (8, 7, NULL, NULL,
                       '',
                       '',
                       'France'),
    (9, 8, NULL, NULL,
                       '',
                       '',
                       'France'),
    (10, 8, NULL, NULL,
                       '',
                       '',
                       'France'),
    (11, 9, NULL, NULL,
                       '',
                       '',
                       'France'),
    (12, 10, NULL, NULL,
                       '',
                       '',
                       'France'),
    (13, 11, NULL, NULL,
                       '',
                       '',
                       'France'),
    (14, 11, NULL, NULL,
                       '',
                       '',
                       'France'),
    (15, 12, NULL, NULL,
                       '2020-01-04',
                       '2020-01-06',
                       'Espagne');

-- --------------------------------------------------------

--
-- Structure de la table campagne
--

DROP TABLE IF EXISTS  campagne ;
CREATE TABLE IF NOT EXISTS  campagne 
(
    identifiant int
(
    11
) NOT NULL AUTO_INCREMENT,
     zone_production  int
(
    11
) DEFAULT NULL,
     campagne  varchar
(
    250
) COLLATE utf8_unicode_ci NOT NULL,
     variete  varchar
(
    100
) COLLATE utf8_unicode_ci NOT NULL,
     n_ordre  varchar
(
    50
) COLLATE utf8_unicode_ci NOT NULL,
     code_parcelle  varchar
(
    100
) COLLATE utf8_unicode_ci NOT NULL,
     poids_brut  double NOT NULL,
     nom_chauffeur  varchar
(
    50
) COLLATE utf8_unicode_ci NOT NULL,
     heure_depart  varchar
(
    10
) COLLATE utf8_unicode_ci NOT NULL,
     heure_arrivee  varchar
(
    10
) COLLATE utf8_unicode_ci NOT NULL,
     immatriculation_camion  varchar
(
    25
) COLLATE utf8_unicode_ci NOT NULL,
     nom_responsable_parcelle  varchar
(
    50
) COLLATE utf8_unicode_ci NOT NULL,
     nombre_cageots  int
(
    11
) NOT NULL,
     poids_net  double NOT NULL,
     date_reception  date NOT NULL,
    PRIMARY KEY
(
    identifiant
),
    KEY  FK_F3E43E25AB9BB300 
(
    zone_production
)
    ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE =utf8_unicode_ci;

--
-- Déchargement des données de la table campagne
--

INSERT INTO  campagne  ( identifiant ,  zone_production ,  campagne ,  variete ,  n_ordre ,  code_parcelle
                          ,  poids_brut ,  nom_chauffeur ,  heure_depart ,  heure_arrivee , 
                          immatriculation_camion ,  nom_responsable_parcelle ,  nombre_cageots ,  poids_net , 
                          date_reception )
VALUES
    (1, 4, 'camp1',
           'DD',
           'ddd',
           'ddddd', 11,
           '11',
           '111',
           '11',
           '11',
           '11', 11, 10,
           '2020-01-12');

-- --------------------------------------------------------

--
-- Structure de la table campagne_tpa
--

DROP TABLE IF EXISTS  campagne_tpa ;
CREATE TABLE IF NOT EXISTS  campagne_tpa 
(
    identifiant int
(
    11
) NOT NULL AUTO_INCREMENT,
     zone_production  int
(
    11
) DEFAULT NULL,
     campagne  varchar
(
    250
) COLLATE utf8_unicode_ci NOT NULL,
     variete  varchar
(
    100
) COLLATE utf8_unicode_ci NOT NULL,
     n_ordre  varchar
(
    50
) COLLATE utf8_unicode_ci NOT NULL,
     code_parcelle  varchar
(
    100
) COLLATE utf8_unicode_ci NOT NULL,
     poids_brut  double NOT NULL,
     nom_chauffeur  varchar
(
    50
) COLLATE utf8_unicode_ci NOT NULL,
     heure_depart  varchar
(
    10
) COLLATE utf8_unicode_ci NOT NULL,
     heure_arrivee  varchar
(
    10
) COLLATE utf8_unicode_ci NOT NULL,
     immatriculation_camion  varchar
(
    25
) COLLATE utf8_unicode_ci NOT NULL,
     nom_responsable_parcelle  varchar
(
    50
) COLLATE utf8_unicode_ci NOT NULL,
     nombre_cageots  int
(
    11
) NOT NULL,
     poids_net  double NOT NULL,
     date_reception  date NOT NULL,
    PRIMARY KEY
(
    identifiant
),
    KEY  FK_F3E43E25AB9BB300 
(
    zone_production
)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE =utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table certificat
--

DROP TABLE IF EXISTS  certificat ;
CREATE TABLE IF NOT EXISTS  certificat 
(
    id int
(
    11
) NOT NULL AUTO_INCREMENT,
     producteur_id  int
(
    11
) DEFAULT NULL,
     numero_cert  int
(
    11
) DEFAULT NULL,
     date_cert  date DEFAULT NULL,
     date_exp  date DEFAULT NULL,
     certificat  varchar
(
    255
) COLLATE utf8_unicode_ci DEFAULT NULL,
    PRIMARY KEY
(
    id
),
    KEY  IDX_27448F77AB9BB300 
(
    producteur_id
)
    ) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8 COLLATE =utf8_unicode_ci;

--
-- Déchargement des données de la table certificat
--

INSERT INTO  certificat  ( id ,  producteur_id ,  numero_cert ,  date_cert ,  date_exp ,  certificat )
VALUES
    (1, 1, NULL, NULL, NULL, 'GLOBALGAP Option 1'),
    (2, 1, NULL, NULL, NULL,
                             'GLOBALGAP Option 2'),
    (3, 1, NULL, NULL, NULL,
                             'Tesco Nature\'s Choice
                             '),
(4, 1, NULL, NULL, NULL, 'Agriculture Biologique
                             '),
(5, 1, NULL, NULL, NULL, 'BRC
                             '),
(6, 1, NULL, NULL, NULL, 'Autres (à préciser)
                             '),
(7, 2, NULL, NULL, NULL, 'GLOBALGAP Option 1 '),
(8, 2, NULL, NULL, NULL, ' GLOBALGAP Option 2 '),
(9, 2, NULL, NULL, NULL, ' Tesco Nature\
                             's Choice'),
    (10, 2, NULL, NULL, NULL,
                             'Agriculture Biologique'),
    (11, 2, NULL, NULL, NULL,
                             'BRC'),
    (12, 2, NULL, NULL, NULL,
                             'Autres (à préciser)'),
    (13, 3, NULL, NULL, NULL,
                             'GLOBALGAP Option 1'),
    (14, 3, NULL, NULL, NULL,
                             'GLOBALGAP Option 2'),
    (15, 3, NULL, NULL, NULL,
                             'Tesco Nature\'s Choice
                             '),
(16, 3, NULL, NULL, NULL, 'Agriculture Biologique
                             '),
(17, 3, NULL, NULL, NULL, 'BRC
                             '),
(18, 3, NULL, NULL, NULL, 'Autres (à préciser)
                             '),
(19, 4, 1, NULL, NULL, 'GLOBALGAP Option 1 '),
(20, 4, 2, NULL, NULL, ' GLOBALGAP Option 2 '),
(21, 4, 3, NULL, NULL, ' Tesco Nature\
                             's Choice'),
    (22, 4, 4, NULL, NULL,
                             'Agriculture Biologique'),
    (23, 4, 5, NULL, NULL,
                             'BRC'),
    (24, 4, 6, NULL, NULL,
                             'Autres (à préciser)'),
    (25, 5, NULL, NULL, NULL,
                             'GLOBALGAP Option 1'),
    (26, 5, NULL, NULL, NULL,
                             'GLOBALGAP Option 2'),
    (27, 5, NULL, NULL, NULL,
                             'Tesco Nature\'s Choice
                             '),
(28, 5, NULL, NULL, NULL, 'Agriculture Biologique
                             '),
(29, 5, NULL, NULL, NULL, 'BRC
                             '),
(30, 5, NULL, NULL, NULL, 'Autres (à préciser)
                             '),
(31, 6, NULL, NULL, NULL, 'GLOBALGAP Option 1 '),
(32, 6, NULL, NULL, NULL, ' GLOBALGAP Option 2 '),
(33, 6, NULL, NULL, NULL, ' Tesco Nature\
                             's Choice'),
    (34, 6, NULL, NULL, NULL,
                             'Agriculture Biologique'),
    (35, 6, NULL, NULL, NULL,
                             'BRC'),
    (36, 6, NULL, NULL, NULL,
                             'Autres (à préciser)'),
    (37, 7, NULL, NULL, NULL,
                             'GLOBALGAP Option 1'),
    (38, 7, NULL, NULL, NULL,
                             'GLOBALGAP Option 2'),
    (39, 7, NULL, NULL, NULL,
                             'Tesco Nature\'s Choice
                             '),
(40, 7, NULL, NULL, NULL, 'Agriculture Biologique
                             '),
(41, 7, NULL, NULL, NULL, 'BRC
                             '),
(42, 7, NULL, NULL, NULL, 'Autres (à préciser)
                             '),
(43, 9, NULL, NULL, NULL, 'GLOBALGAP Option 1 '),
(44, 9, NULL, NULL, NULL, ' GLOBALGAP Option 2 '),
(45, 9, NULL, NULL, NULL, ' Tesco Nature\
                             's Choice'),
    (46, 9, NULL, NULL, NULL,
                             'Agriculture Biologique'),
    (47, 9, NULL, NULL, NULL,
                             'BRC'),
    (48, 9, NULL, NULL, NULL,
                             'Autres (à préciser)'),
    (49, 10, NULL, NULL, NULL,
                             'GLOBALGAP Option 1'),
    (50, 10, NULL, NULL, NULL,
                             'GLOBALGAP Option 2'),
    (51, 10, NULL, NULL, NULL,
                             'Tesco Nature\'s Choice
                             '),
(52, 10, NULL, NULL, NULL, 'Agriculture Biologique
                             '),
(53, 10, NULL, NULL, NULL, 'BRC
                             '),
(54, 10, NULL, NULL, NULL, 'Autres (à préciser)
                             '),
(55, 11, NULL, NULL, NULL, 'GLOBALGAP Option 1 '),
(56, 11, NULL, NULL, NULL, ' GLOBALGAP Option 2 '),
(57, 11, NULL, NULL, NULL, ' Tesco Nature\
                             's Choice'),
    (58, 11, NULL, NULL, NULL,
                             'Agriculture Biologique'),
    (59, 11, NULL, NULL, NULL,
                             'BRC'),
    (60, 11, NULL, NULL, NULL,
                             'Autres (à préciser)'),
    (61, 12, NULL, NULL, NULL,
                             'GLOBALGAP Option 1'),
    (62, 12, NULL, NULL, NULL,
                             'GLOBALGAP Option 2'),
    (63, 12, NULL, NULL, NULL,
                             'Tesco Nature\'s Choice
                             '),
(64, 12, NULL, NULL, NULL, 'Agriculture Biologique
                             '),
(65, 12, NULL, NULL, NULL, 'BRC
                             '),
(66, 12, NULL, NULL, NULL, 'Autres (à préciser)
                             ');

-- --------------------------------------------------------

--
-- Structure de la table demande
--

DROP TABLE IF EXISTS demande;
CREATE TABLE IF NOT EXISTS demande (
  id int(11) NOT NULL AUTO_INCREMENT,
  producteur_id int(11) DEFAULT NULL,
  typeDemande varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  dateDepot datetime DEFAULT NULL,
  statut int(11) NOT NULL,
  PRIMARY KEY (id),
  KEY producteur_id (producteur_id)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table demande
--

INSERT INTO demande (id, producteur_id, typeDemande, dateDepot, statut) VALUES
(1, 1, 'Nouvelle
                             ', '2019-11-30 13:30:40 ', 1),
(2, 2, ' Nouvelle
                             ', '2019-11-30 13:33:14 ', 2),
(3, 3, ' Nouvelle
                             ', '2020-01-05 13:32:36 ', 2),
(4, 4, ' Nouvelle
                             ', '2020-01-05 18:53:14 ', 2),
(5, 5, ' Nouvelle
                             ', '2020-01-05 19:18:07 ', 2),
(6, 6, ' Nouvelle
                             ', '2020-01-05 19:28:37 ', 2),
(7, 7, ' Nouvelle
                             ', '2020-01-05 19:39:22 ', 2),
(8, 8, ' Nouvelle
                             ', '2020-01-05 19:44:40 ', 2),
(9, 9, ' Nouvelle
                             ', '2020-01-05 19:50:11 ', 2),
(10, 10, ' Nouvelle
                             ', '2020-01-05 19:52:44 ', 0),
(11, 11, ' Nouvelle
                             ', '2020-01-18 09:35:59 ', 0),
(12, 12, ' Nouvelle
                             ', '2020-01-19 16:35:55 ', 0);

-- --------------------------------------------------------

--
-- Structure de la table demande_inspection
--

DROP TABLE IF EXISTS demande_inspection;
CREATE TABLE IF NOT EXISTS demande_inspection (
  id int(11) NOT NULL AUTO_INCREMENT,
  nom_prenom_demandeur varchar(35) COLLATE utf8_unicode_ci DEFAULT NULL,
  statuts int(11) NOT NULL,
  nom_entreprise varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  numero_telephone_demandeur varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  date_appel varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  zone_provenance varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  proprietaire_verger varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  proprietaire_verger1 varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  proprietaire_verger2 varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  lieu_conditionnement varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  date_conditionnement varchar(15) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table deposerdemande
--

DROP TABLE IF EXISTS deposerdemande;
CREATE TABLE IF NOT EXISTS deposerdemande (
  demande_id int(11) NOT NULL,
  producteur_id int(11) NOT NULL,
  PRIMARY KEY (demande_id,producteur_id),
  KEY IDX_21CAAE6880E95E18 (demande_id),
  KEY IDX_21CAAE68AB9BB300 (producteur_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table detail_lot
--

DROP TABLE IF EXISTS detail_lot;
CREATE TABLE IF NOT EXISTS detail_lot (
  identifiant int(11) NOT NULL AUTO_INCREMENT,
  id_campagne int(11) DEFAULT NULL,
  id_lot int(11) DEFAULT NULL,
  calibre varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  nombre_carton int(11) NOT NULL,
  poids_net double NOT NULL,
  n_palette int(11) NOT NULL,
  PRIMARY KEY (identifiant),
  KEY FK5qbslk1ioudummgell60ha644478 (id_lot),
  KEY FK5qbslk1ioudummg (id_campagne)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table detail_lot_tpa
--

DROP TABLE IF EXISTS detail_lot_tpa;
CREATE TABLE IF NOT EXISTS detail_lot_tpa (
  identifiant int(11) NOT NULL AUTO_INCREMENT,
  id_lot int(11) DEFAULT NULL,
  id_campagne int(11) DEFAULT NULL,
  calibre varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  nombre_carton int(11) NOT NULL,
  poids_net double NOT NULL,
  n_palette int(11) NOT NULL,
  PRIMARY KEY (identifiant),
  KEY FK5qbslk1ioudummg (id_campagne),
  KEY FK5qbslk1ioudummgell60ha644478 (id_lot)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table exportation
--

DROP TABLE IF EXISTS exportation;
CREATE TABLE IF NOT EXISTS exportation (
  id int(11) NOT NULL AUTO_INCREMENT,
  denomEntreprise varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  prenoNomInspecteur varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  dateInspection varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  lieuInspection varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  personneRencontre varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  valide int(11) NOT NULL,
  fonctionpersonneRencontre varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  variete varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  categorie varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  qteDeclare varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  nbColis varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  typeTransport varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  tailleEchantillon varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  nbColiControle varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  nbMangueControle varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  registreReceptionMangue varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  declarationSupp varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  marqueColis varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  conformiteColis varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  numeroLot varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  conformiteLot varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  numeroPalette varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  conformitePalette varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  maturite varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  piqureMoucheFruit varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  pourriturePendonculaire varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  charanconNoyaux varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  traceArthrocnose varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  marquageNIMP15 varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  etatSanitaireGeneral varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  commentaire varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table fiche_exportation
--

DROP TABLE IF EXISTS fiche_exportation;
CREATE TABLE IF NOT EXISTS fiche_exportation (
  id int(11) NOT NULL AUTO_INCREMENT,
  id_producteur int(11) DEFAULT NULL,
  id_palettisation int(11) DEFAULT NULL,
  id_campage int(11) DEFAULT NULL,
  numero_exporation int(11) NOT NULL,
  date_exportation datetime NOT NULL,
  moyen_utilise varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  pays_exportation varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  nom_compagnie varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  nom_recepteur varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  commentaire varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  date_reception datetime DEFAULT NULL,
  statut int(11) NOT NULL,
  variete varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  photo varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FK_F3E43E25A255zza (id_producteur),
  KEY FK_F3E43E25A2bff47 (id_campage),
  KEY FK_F3E43E25A25dd58d4 (id_palettisation)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table fiche_operateur
--

DROP TABLE IF EXISTS fiche_operateur;
CREATE TABLE IF NOT EXISTS fiche_operateur (
  id int(11) NOT NULL AUTO_INCREMENT,
  conditionnement_id int(11) DEFAULT NULL,
  tableTravail varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  chambreFroide varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  temperature varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  conformite varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  emballage varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  sanitaire varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  securite varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  pharmacie varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  lavage varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  installation varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  marcheEnAvant varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  qualiteCond varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  docArchive varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  MaterielAutoControle varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  responsableQualite varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  niveauResponsable varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  marquageEmballage varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  uniteTraitementNTMP varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  paletteEmbTraite varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  paletteEmbRecup varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  supportEnreg varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  supportAJour varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  note varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteChambre varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteSysteme varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteConformiteTemp varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteUniteTrait varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteSanitaire varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  afficheSecurite varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  notePharmacie varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  notelavageMain varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteConformiteInstallation varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteMarcheAvant varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteQualiteCond varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteDocArchivage varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteMaterielAutoControle varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteResponsableQualite varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteNiveauResponsable varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  typeCertificat varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteCertificat varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  tesco varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  bio varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  globalGap varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id),
  KEY IDX_33D94475A222637 (conditionnement_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table fos_user
--

DROP TABLE IF EXISTS fos_user;
CREATE TABLE IF NOT EXISTS fos_user (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  username_canonical varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  email_canonical varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  enabled tinyint(1) NOT NULL,
  salt varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  password varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  last_login datetime DEFAULT NULL,
  confirmation_token varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  password_requested_at datetime DEFAULT NULL,
  roles longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type: array)
                             ',
  nom varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  prenom varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  producteur_id varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY UNIQ_957A647992FC23A8 (username_canonical),
  UNIQUE KEY UNIQ_957A6479A0D96FBF (email_canonical),
  UNIQUE KEY UNIQ_957A6479C05FB297 (confirmation_token)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table fos_user
--

INSERT INTO fos_user (id, username, username_canonical, email, email_canonical, enabled, salt, password, last_login, confirmation_token, password_requested_at, roles, nom, prenom, producteur_id) VALUES
(3, 'admin
                             ', 'admin
                             ', 'admin @mmm
                             ', 'admin @mmm
                             ', 1, NULL, '$2y$
                             13$IrLVmLk.Klq2IjE7v68UnuDpn9FeiMjDG4AoXqhvn36qJphP/Zxke', '2020-01-19 19:06:17', NULL, NULL, 'a:5:{i:0;s:15:\"ROLE_PRODUCTEUR\";i:1;s:10:\"ROLE_AGENT\";i:2;s:15:\"ROLE_CONTROLEUR\";i:3;s:10:\"ROLE_ADMIN\";i:4;s:16:\"ROLE_EXPORTATEUR\";}', 'admin', 'admin', NULL),
(4, 'adminuser', 'adminuser', 'papa@dd.com', 'papa@dd.com', 1, NULL, '$2y$ 13 $/5.qloFodJAYUo2xZVQ.MOKv2dfrBYi8s8LT4tBk61N.iO/ThPU3e
                             ', '2020-01-05 20:08:22 ', NULL, NULL, ' a:1:{i:0; s :16:\
                             "ROLE_SUPER_ADMIN\";}
                             ', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table fos_user_tpa
--

DROP TABLE IF EXISTS fos_user_tpa;
CREATE TABLE IF NOT EXISTS fos_user_tpa (
  id int(11) NOT NULL AUTO_INCREMENT,
  username varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  username_canonical varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  email varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  email_canonical varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  enabled tinyint(1) NOT NULL,
  salt varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  password varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  last_login datetime DEFAULT NULL,
  confirmation_token varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  password_requested_at datetime DEFAULT NULL,
  roles longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type: array)
                             ',
  PRIMARY KEY (id),
  UNIQUE KEY UNIQ_957A647992FC23A8 (username_canonical),
  UNIQUE KEY UNIQ_957A6479A0D96FBF (email_canonical),
  UNIQUE KEY UNIQ_957A6479C05FB297 (confirmation_token)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table gestion_qualite
--

DROP TABLE IF EXISTS gestion_qualite;
CREATE TABLE IF NOT EXISTS gestion_qualite (
  id int(11) NOT NULL AUTO_INCREMENT,
  systemGesQual varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  gestionInterne varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  consultant varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  autre varchar(3) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteAutre varchar(25) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table inspection_mangue
--

DROP TABLE IF EXISTS inspection_mangue;
CREATE TABLE IF NOT EXISTS inspection_mangue (
  id int(11) NOT NULL AUTO_INCREMENT,
  date varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  referenceNotification varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  exportateur varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  numeroEnregistrement varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  motifNotification varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  quantite varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  refCertifPhyto varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  refFichInspection varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  equipeInvestig varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  conditionnement_id int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY IDX_6AC7E186A222637 (conditionnement_id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table lot
--

DROP TABLE IF EXISTS lot;
CREATE TABLE IF NOT EXISTS lot (
  identifiant int(11) NOT NULL AUTO_INCREMENT,
  id_campagne int(11) DEFAULT NULL,
  code_lot varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  n_lot_recolte varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  nombre_cageots int(11) NOT NULL,
  poids_brut double NOT NULL,
  poids_net double NOT NULL,
  PRIMARY KEY (identifiant),
  KEY FK5qbslk1ioudummgell60ha65585587 (id_campagne)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table lot
--

INSERT INTO lot (identifiant, id_campagne, code_lot, n_lot_recolte, nombre_cageots, poids_brut, poids_net) VALUES
(1, NULL, '123
                             ', '123
                             ', 2, 41, 46),
(2, NULL, '123
                             ', '123
                             ', 2, 41, 46);

-- --------------------------------------------------------

--
-- Structure de la table lot_tpa
--

DROP TABLE IF EXISTS lot_tpa;
CREATE TABLE IF NOT EXISTS lot_tpa (
  identifiant int(11) NOT NULL AUTO_INCREMENT,
  id_campagne int(11) DEFAULT NULL,
  code_lot varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  n_lot_recolte varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  nombre_cageots int(11) NOT NULL,
  poids_brut double NOT NULL,
  poids_net double NOT NULL,
  PRIMARY KEY (identifiant),
  KEY FK5qbslk1ioudummgell60ha65585587 (id_campagne)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table personne
--

DROP TABLE IF EXISTS personne;
CREATE TABLE IF NOT EXISTS personne (
  id int(11) NOT NULL AUTO_INCREMENT,
  nom varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  prenom varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  statut varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  num_tel varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  num_fixe varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  num_fax varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table personnes
--

DROP TABLE IF EXISTS personnes;
CREATE TABLE IF NOT EXISTS personnes (
  id int(11) NOT NULL AUTO_INCREMENT,
  nom varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  prenom varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  num_tel varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  num_fixe varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  num_fax varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  statut varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table producteur
--

DROP TABLE IF EXISTS producteur;
CREATE TABLE IF NOT EXISTS producteur (
  id int(11) NOT NULL AUTO_INCREMENT,
  nom varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  prenom varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  addresse varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  statutLegal varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  ninea varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  siegeSocial varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  region varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  ville varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  departement varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  commune varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  arrondissement varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  village varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  numTel varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  numFixe varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  fex varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  email varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  prenomRep varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  nomRep varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  addresseRep varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  fonctionRep varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  mobileRep varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  fixeRep varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  faxeRep varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  emailRep varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  nomManager varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  prenomManager varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  titreManager varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  telManager varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  emailManager varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  conditionnement varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  exportation varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  vulgarisation varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  bordChamp varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  europe varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  regie varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  enStation varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  autre varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  gestionInterne varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  consultant varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  date datetime DEFAULT NULL,
  noteProduction varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteExportation varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  noteCond varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  notePrestation varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table producteur
--

INSERT INTO producteur (id, nom, prenom, addresse, statutLegal, ninea, siegeSocial, region, ville, departement, commune, arrondissement, village, numTel, numFixe, fex, email, prenomRep, nomRep, addresseRep, fonctionRep, mobileRep, fixeRep, faxeRep, emailRep, nomManager, prenomManager, titreManager, telManager, emailManager, conditionnement, exportation, vulgarisation, bordChamp, europe, regie, enStation, autre, gestionInterne, consultant, date, noteProduction, noteExportation, noteCond, notePrestation) VALUES
(1, 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, '123456
                             ', NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, NULL, NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.coms
                             ', 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.coms
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, NULL, NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, NULL, NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1
                             ', NULL, 'Allemagne
                             ', '1
                             ', NULL, NULL, NULL, NULL, NULL, 'Commentaires
                             ', 'Commentaires
                             ', NULL, NULL),
(5, 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, NULL, NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, NULL, NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1
                             ', NULL, 'Belgique
                             ', '1
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, NULL, NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, NULL, NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, NULL, NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, NULL, NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, NULL, NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 'COUNDIA
                             ', 'Papa
                             ', NULL, NULL, NULL, NULL, 'Dakar
                             ', NULL, 'Rufisque
                             ', NULL, NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', 'Papa
                             ', 'COUNDIA
                             ', NULL, NULL, '775392482
                             ', NULL, NULL, 'papacoundia@gmail.com
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1
                             ', NULL, NULL, '1
                             ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table registre_palettisation
--

DROP TABLE IF EXISTS registre_palettisation;
CREATE TABLE IF NOT EXISTS registre_palettisation (
  id int(11) NOT NULL AUTO_INCREMENT,
  code_lot int(11) DEFAULT NULL,
  id_campagne int(11) DEFAULT NULL,
  date date NOT NULL,
  n_palette int(11) NOT NULL,
  nbre_cartons int(11) NOT NULL,
  total_cartons int(11) NOT NULL,
  heure_entree_ch_froide int(11) NOT NULL,
  heure_sortie_ch_froide int(11) NOT NULL,
  destinataire varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  parcking_liste varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  paragraphe varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  produit_calibre varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  statut int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FK5qbslk1ioudummgell60ha6 (id_campagne),
  KEY FK5qbslk1ioudummgell60ha6hhgfd (code_lot)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table registre_palettisation_tpa
--

DROP TABLE IF EXISTS registre_palettisation_tpa;
CREATE TABLE IF NOT EXISTS registre_palettisation_tpa (
  id int(11) NOT NULL AUTO_INCREMENT,
  code_lot int(11) DEFAULT NULL,
  id_campagne int(11) DEFAULT NULL,
  date date NOT NULL,
  n_palette int(11) NOT NULL,
  nbre_cartons int(11) NOT NULL,
  total_cartons int(11) NOT NULL,
  heure_entree_ch_froide int(11) NOT NULL,
  heure_sortie_ch_froide int(11) NOT NULL,
  destinataire varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  parcking_liste varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  paragraphe varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  produit_calibre varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  statut int(11) DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FK5qbslk1ioudummgell60ha6 (id_campagne),
  KEY FK5qbslk1ioudummgell60ha6hhgfd (code_lot)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table registre_reception
--

DROP TABLE IF EXISTS registre_reception;
CREATE TABLE IF NOT EXISTS registre_reception (
  ordre_arrive int(11) NOT NULL AUTO_INCREMENT,
  id_campagne int(11) DEFAULT NULL,
  parcelle_origine varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  date_recolte date NOT NULL,
  variete varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  nombre_cageots int(11) NOT NULL,
  poids_brut int(11) NOT NULL,
  poids_net int(11) NOT NULL,
  code_attribue varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (ordre_arrive),
  KEY FK5qbslk1ioudummgell60ha69j (id_campagne)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table registre_reception_tpa
--

DROP TABLE IF EXISTS registre_reception_tpa;
CREATE TABLE IF NOT EXISTS registre_reception_tpa (
  ordre_arrive int(11) NOT NULL AUTO_INCREMENT,
  id_campagne int(11) DEFAULT NULL,
  parcelle_origine varchar(250) COLLATE utf8_unicode_ci NOT NULL,
  date_recolte date NOT NULL,
  variete varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  nombre_cageots int(11) NOT NULL,
  poids_brut int(11) NOT NULL,
  poids_net int(11) NOT NULL,
  code_attribue varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (ordre_arrive),
  KEY FK5qbslk1ioudummgell60ha69j (id_campagne)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table site_conditionnement
--

DROP TABLE IF EXISTS site_conditionnement;
CREATE TABLE IF NOT EXISTS site_conditionnement (
  id int(11) NOT NULL AUTO_INCREMENT,
  producteur_id int(11) DEFAULT NULL,
  speculation varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  siteCond varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  zones varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  contrat_prestation varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  contrat_location varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  lacalisation varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id),
  KEY IDX_31D134D1AB9BB300 (producteur_id)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table site_conditionnement
--

INSERT INTO site_conditionnement (id, producteur_id, speculation, siteCond, zones, contrat_prestation, contrat_location, lacalisation) VALUES
(1, 5, NULL, 'ET&DS
                             ', 'CENTRE
                             ', 'non
                             ', 'non
                             ', 'village de fimla
                             '),
(2, 12, NULL, 'SAFINA
                             ', 'NIAYES
                             ', 'non
                             ', 'non
                             ', 'SEBIKOTANE vers la SDE
                             ');

-- --------------------------------------------------------

--
-- Structure de la table site_production
--

DROP TABLE IF EXISTS site_production;
CREATE TABLE IF NOT EXISTS site_production (
  id int(11) NOT NULL AUTO_INCREMENT,
  producteur_id int(11) DEFAULT NULL,
  perimetre varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  prod_associe varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  piste_associe varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  localisation varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  superficie varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  speculation varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  nb_spec varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  regie varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FK_F3E43E25AB9BB3002558 (producteur_id)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table site_production
--

INSERT INTO site_production (id, producteur_id, perimetre, prod_associe, piste_associe, localisation, superficie, speculation, nb_spec, regie) VALUES
(1, 4, 'v1
                             ', NULL, NULL, 'village de fimla
                             ', '12
                             ', NULL, '2
                             ', NULL),
(2, 4, 'v2
                             ', NULL, NULL, 'Arrondissement de Kataba 1 ', ' 10 ', NULL, ' 2 ', NULL),
(3, 5, ' dd
                             ', NULL, NULL, 'village de fimla
                             ', 'dd
                             ', NULL, '2
                             ', NULL),
(4, 5, 'dd
                             ', NULL, NULL, 'Arrondissement de Kataba 1 ', ' dd
                             ', NULL, '2
                             ', NULL),
(5, 6, 'v1
                             ', NULL, NULL, 'Arrondissement de Kataba 1 ', ' 10 ', NULL, ' 2 ', NULL),
(6, 6, ' v2
                             ', NULL, NULL, 'Arrondissement de Kataba 1 ', ' 12 ', NULL, ' 2 ', NULL),
(7, 7, ' setSpeculation
                             ', NULL, NULL, 'Arrondissement de Kataba 1 ', ' 10 ', NULL, ' 2 ', NULL),
(8, 7, ' setSpeculation
                             ', NULL, NULL, 'Arrondissement de Kataba 1 ', ' 12 ', NULL, ' 2 ', NULL),
(9, 8, ' c
                             ', NULL, NULL, 'Arrondissement de Kataba 1 ', ' ccc
                             ', NULL, '2
                             ', NULL),
(10, 8, 'cccc
                             ', NULL, NULL, 'Arrondissement de Kataba 1 ', ' cc
                             ', NULL, '2
                             ', NULL),
(11, 9, 'v1
                             ', NULL, NULL, 'Arrondissement de Kataba 1 ', ' 10 ', ' Mais doux
                             ', '1
                             ', NULL),
(12, 10, 'v1
                             ', NULL, NULL, 'Arrondissement de Kataba 1 ', ' 12 ', ' Mais doux
                             ', '1
                             ', NULL),
(13, 12, 'poi
                             ', NULL, NULL, 'SEBIKOTANE vers la SDE
                             ', '12
                             ', 'Mangue
                             ', '1
                             ', NULL);

-- --------------------------------------------------------

--
-- Structure de la table site_production_tpa
--

DROP TABLE IF EXISTS site_production_tpa;
CREATE TABLE IF NOT EXISTS site_production_tpa (
  id int(11) NOT NULL AUTO_INCREMENT,
  producteur_id int(11) DEFAULT NULL,
  perimetre varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  prod_associe varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  piste_associe varchar(5) COLLATE utf8_unicode_ci DEFAULT NULL,
  localisation varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  superficie varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  speculation varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  nb_spec varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  regie varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (id),
  KEY FK_F3E43E25AB9BB3002558 (producteur_id)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table site_production_tpa
--

INSERT INTO site_production_tpa (id, producteur_id, perimetre, prod_associe, piste_associe, localisation, superficie, speculation, nb_spec, regie) VALUES
(1, 11, 'verger 1 ', NULL, NULL, ' Diouloulou
                             ', '100
                             ', NULL, '2
                             ', NULL),
(2, 11, 'verger 2 ', NULL, NULL, ' 2 ', ' 200 ', NULL, ' 2 ', NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table calendrier_production
--
ALTER TABLE calendrier_production
  ADD CONSTRAINT FK_C8483468AB9BB300 FOREIGN KEY (producteur_id) REFERENCES producteur (id);

--
-- Contraintes pour la table campagne
--
ALTER TABLE campagne
  ADD CONSTRAINT FK_539B5D16F30CA200 FOREIGN KEY (zone_production) REFERENCES site_production (id);

--
-- Contraintes pour la table campagne_tpa
--
ALTER TABLE campagne_tpa
  ADD CONSTRAINT FK_8599CE63F30CA200 FOREIGN KEY (zone_production) REFERENCES site_production_tpa (id);

--
-- Contraintes pour la table certificat
--
ALTER TABLE certificat
  ADD CONSTRAINT FK_27448F77AB9BB300 FOREIGN KEY (producteur_id) REFERENCES producteur (id);

--
-- Contraintes pour la table demande
--
ALTER TABLE demande
  ADD CONSTRAINT FK_2694D7A5AB9BB300 FOREIGN KEY (producteur_id) REFERENCES producteur (id);

--
-- Contraintes pour la table detail_lot
--
ALTER TABLE detail_lot
  ADD CONSTRAINT FK_3C3EFB8F340B183 FOREIGN KEY (id_campagne) REFERENCES campagne (identifiant),
  ADD CONSTRAINT FK_3C3EFB8F9525C141 FOREIGN KEY (id_lot) REFERENCES lot (identifiant);

--
-- Contraintes pour la table fiche_exportation
--
ALTER TABLE fiche_exportation
  ADD CONSTRAINT FK_D1A722F86EEB2649 FOREIGN KEY (id_campage) REFERENCES campagne (identifiant),
  ADD CONSTRAINT FK_D1A722F88CF4D98B FOREIGN KEY (id_palettisation) REFERENCES registre_palettisation (id),
  ADD CONSTRAINT FK_D1A722F8DB516EC7 FOREIGN KEY (id_producteur) REFERENCES producteur (id);

--
-- Contraintes pour la table fiche_operateur
--
ALTER TABLE fiche_operateur
  ADD CONSTRAINT FK_33D94475A222637 FOREIGN KEY (conditionnement_id) REFERENCES site_conditionnement (id);

--
-- Contraintes pour la table lot
--
ALTER TABLE lot
  ADD CONSTRAINT FK_B81291B340B183 FOREIGN KEY (id_campagne) REFERENCES campagne (identifiant);

--
-- Contraintes pour la table lot_tpa
--
ALTER TABLE lot_tpa
  ADD CONSTRAINT FK_F196F52F340B183 FOREIGN KEY (id_campagne) REFERENCES campagne_tpa (identifiant);

--
-- Contraintes pour la table registre_palettisation
--
ALTER TABLE registre_palettisation
  ADD CONSTRAINT FK_A9C0C1C5340B183 FOREIGN KEY (id_campagne) REFERENCES campagne (identifiant),
  ADD CONSTRAINT FK_A9C0C1C5E3A11EB1 FOREIGN KEY (code_lot) REFERENCES lot (identifiant);

--
-- Contraintes pour la table registre_palettisation_tpa
--
ALTER TABLE registre_palettisation_tpa
  ADD CONSTRAINT FK_4156061D340B183 FOREIGN KEY (id_campagne) REFERENCES campagne_tpa (identifiant),
  ADD CONSTRAINT FK_4156061DE3A11EB1 FOREIGN KEY (code_lot) REFERENCES lot_tpa (identifiant);

--
-- Contraintes pour la table registre_reception
--
ALTER TABLE registre_reception
  ADD CONSTRAINT FK_276945A3340B183 FOREIGN KEY (id_campagne) REFERENCES campagne (identifiant);

--
-- Contraintes pour la table site_conditionnement
--
ALTER TABLE site_conditionnement
  ADD CONSTRAINT FK_31D134D1AB9BB300 FOREIGN KEY (producteur_id) REFERENCES producteur (id);

--
-- Contraintes pour la table site_production
--
ALTER TABLE site_production
  ADD CONSTRAINT FK_F3E43E25AB9BB300 FOREIGN KEY (producteur_id) REFERENCES producteur (id);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
