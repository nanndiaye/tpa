<?php

namespace NosFormulairesBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    /**
     * @Route("/",name="home_page")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig');
    }
}
