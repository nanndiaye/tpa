<?php

namespace TpaBundle\Controller;

use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\PhpWord;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use TpaBundle\Entity\Exportation;
use TpaBundle\Entity\FicheOperateur;
use TpaBundle\Entity\InspectionMangue;
use TpaBundle\Entity\SiteConditionnement;

/**
 * Dpv controller.
 *
 * @Route("dpv")
 */
class DpvController extends Controller
{
    /**
     *
     * @Route("/ficheOperateur", name="fiche_operateur")
     * @Method("GET")
     */
    public function ficheOperateurAction(Request $request)
    {
       
     
        $em = $this->getDoctrine()->getManager();
       // dump($em);die();
        $site = $em->getRepository('TpaBundle:SiteConditionnement')->findAll();
        //dump($site);die();
        return $this->render('dpv/ficheOperateur.html.twig', array(
            'ficheope' => $site,

        ));
    }
    /**
     *
     * @Route("/ficheExportationMangue", name="inspection_exportation_mangue")
     * @Method({"GET", "POST"})
     */
    public function enregistrerficheExportationMangueAction(Request $request)
    {
        dump($request);die();
        $ficheExportation=new Exportation();
        $ficheExportation->setDenomEntreprise($request->request->get('denomEntreprise'));
        $ficheExportation->setPrenoNomInspecteur($request->request->get('prenomNomInspecteur'));
        $ficheExportation->setDateInspection($request->request->get('dateInspection'));
        $ficheExportation->setLieuInspection($request->request->get('lieuInspection'));
        $ficheExportation->setPersonneRencontre($request->request->get('personneRencontre'));
        $ficheExportation->setFonctionpersonneRencontre($request->request->get('fonctionPersonneRencotre'));
        $ficheExportation->setVariete($request->request->get('variete'));
        $ficheExportation->setCategorie($request->request->get('categorie'));
        $ficheExportation->setQteDeclare($request->request->get('qteDeclare'));
        $ficheExportation->setNbColis($request->request->get('nbColis'));
        $ficheExportation->setTypeTransport($request->request->get('typeTransport'));
        $ficheExportation->setTailleEchantillon($request->request->get('tailleEchantillon'));
        $ficheExportation->setNbColiControle($request->request->get('nbColisControle'));
        $ficheExportation->setNbMangueControle($request->request->get('nbMangueControle'));
        $ficheExportation->setRegistreReceptionMangue($request->request->get('registreReceptionMangue'));
        $ficheExportation->setDeclarationSupp($request->request->get('declarationSupplementaire'));
        $ficheExportation->setMarqueColis($request->request->get('marqueColis'));
        $ficheExportation->setConformiteColis($request->request->get('conformiteMarqueColis'));
        $ficheExportation->setNumeroLot($request->request->get('numeroLot'));
        $ficheExportation->setConformiteLot($request->request->get('conformiteNumLot'));
        $ficheExportation->setNumeropalette($request->request->get('numPalette'));
        $ficheExportation->setConformitePalette($request->request->get('conformiteNumPalette'));
        $ficheExportation->setMaturite($request->request->get('maturite'));
        $ficheExportation->setPiqureMoucheFruit($request->request->get('piqureMoucheFruit'));
        $ficheExportation->setPourriturePendonculaire($request->request->get('pourriturePedonculaire'));
        $ficheExportation->setCharanconNoyaux($request->request->get('charanconNoyaux'));
        $ficheExportation->setTraceArthrocnose($request->request->get('traceArthrocnose'));
        $ficheExportation->setMarquageNIMP15($request->request->get('marquageNIMP15'));
        $ficheExportation->setEtatSanitaireGeneral($request->request->get('etatSanitaireGeneral'));
                $ficheExportation->setValide(0);
        $ficheExportation->setCommentaire($request->request->get('commentaire'));
        //var_dump($request->request->get('maturite'));die();
        dump($ficheExportation);
        die();
        $em = $this->getDoctrine()->getManager();
        $em->persist($ficheExportation);
        $em->flush();
        return $this->render('dpv/ficheExportateur.html.twig', array(
            'ficheExportateur' => $ficheExportation,

        ));
    }

    /**
     *
     * @Route("/ficheInspectionOperateur", name="fiche_inspection_operateur")
     * @Method({"GET", "POST"})
     */
    public function enregistrerficheOperateurAction(Request $request)
    {
      
        $ficheOP=new FicheOperateur();
            $ficheOP->setTesco($request->get('type_1'));
            $ficheOP->setBio($request->get('type_2'));
            $ficheOP->setGlobalGap($request->get('type_3'));
            $ficheOP->setNoteCertificat($request->get('note_cert'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($ficheOP);
            $em->flush();


        for ($i=1;$i<=4;$i++){
            $ficheOP->setTypeCertificat($request->get('produit_'.$i));

        }
        //dump($ficheOP);die();
        $site=new SiteConditionnement();
        $site = $em->getRepository('TpaBundle:SiteConditionnement')->findBy(array('id'=>$request->request->get('sitecond')));
       // dump($site);die();
     
        $ficheOP->setConditionnement($site->getId());
                $ficheOP->setTableTravail($request->request->get('Travail'));

                $ficheOP->setChambreFroide($request->request->get('chambre'));

                $ficheOP->setTemperature($request->request->get('temperature'));
          
    
                $ficheOP->setConformite($request->request->get('conformite'));
        
                $ficheOP->setEmballage($request->request->get('emballage'));
        
                $ficheOP->setSanitaire($request->request->get('sanitaire'));
         
                $ficheOP->setSecurite($request->request->get('securite'));
          
      
                $ficheOP->setPharmacie($request->request->get('pharmacie'));
      
                $ficheOP->setLavage($request->request->get('lavage'));
        
                $ficheOP->setInstallation($request->request->get('installation'));

                $ficheOP->setMarcheEnAvant($request->request->get('marcheEnAvant'));
       
                $ficheOP->setQualiteCond($request->request->get('qualiteCond'));
          
                $ficheOP->setDocArchive($request->request->get('docArchive'));
          
                $ficheOP->setMaterielAutoControle($request->request->get('MaterielAutoControle'));
        
                $ficheOP->setResponsableQualite($request->request->get('responsableQualite'));
          

                $ficheOP->setNiveauResponsable($request->request->get('NiveauResp'));
         
                $ficheOP->setSupportEnreg($request->request->get('supportEnreg'));
           
                $ficheOP->setSupportAJour($request->request->get('supportAjour'));
                $ficheOP->setMarquageEmballage($request->request->get('marquageEmb'));
           
         
                $ficheOP->setUniteTraitementNTMP($request->request->get('uniteTraitementNTMP'));
      
                $ficheOP->setPaletteEmbTraite($request->request->get('paletteEmbTraite'));
          
                //$ficheOP->setPaletteEmbRecup('OUI');
        
                $ficheOP->setQualiteCond($request->request->get('qualiteCond'));
           
            $ficheOP->setNote($request->request->get('noteTravail'));
            $ficheOP->setNoteChambre($request->request->get('noteChambre'));
            $ficheOP->setNoteSysteme($request->request->get('noteTemperature'));
            $ficheOP->setNoteConformiteTemp($request->request->get('noteConformite'));
            $ficheOP->setNoteUniteTrait($request->request->get('noteEmballage'));
            $ficheOP->setNoteSanitaire($request->request->get('noteSanitaire'));
            $ficheOP->setAfficheSecurite($request->request->get('noteSecurite'));
            $ficheOP->setNotePharmacie($request->request->get('notePharmacie'));
            $ficheOP->setNotelavageMain($request->request->get('noteLavage'));
            $ficheOP->setNoteConformiteInstallation($request->request->get('noteInstallation'));
            $ficheOP->setNoteMarcheAvant($request->request->get('notemarcheEnAvant'));
            $ficheOP->setNoteQualiteCond($request->request->get('notequaliteCond'));
            $ficheOP->setNoteDocArchivage($request->request->get('notedocArchive'));
            $ficheOP->setNoteMaterielAutoControle($request->request->get('noteMaterielAutoControle'));
            $ficheOP->setNoteResponsableQualite($request->request->get('noteresponsableQualite'));
            $ficheOP->setNoteNiveauResponsable($request->request->get('noteNiveauResp'));
           // $ficheOP->setNoteUniteTraitementNTMP($request->request->get('  noteuniteTraitementNTMP'));
          
//            $ficheOP->setConditionnement($request->request->get('conditionnement'));
            $em = $this->getDoctrine()->getManager();
           
            $em->persist($ficheOP);
            $em->flush();


        //}

        return $this->render('dpv/ficheOperateur.html.twig', array(
            'ficheOperateur' => $ficheOP,

        ));
    }

    /**
     *
     * @Route("/inspectionOperateurMangue", name="inspection_operateur_mangue")
     * @Method({"GET", "POST"})
     */
    public function enregistrerficheInspectionMangueAction(Request $request)
    {
        $inspectionMangue=new InspectionMangue();


        $inspectionMangue->setTesco($request->get('type_1'));
        $inspectionMangue->setBio($request->get('type_2'));
        $inspectionMangue->setGlobalGap($request->get('type_3'));
        $inspectionMangue->setNoteCertificat($request->get('notes_1'));
        $em = $this->getDoctrine()->getManager();
        $em->persist($inspectionMangue);
        $em->flush();


        //var_dump('test');die();
        for ($i=1;$i<=15;$i++){
            $inspectionMangue->setTypeCertificat($request->get('produit_'.$i));

        }

      

        return $this->render('dpv/ficheOperateur.html.twig', array(
            'ficheOperateur' => $inspectionMangue,

        ));
    }





    /**
     *
     * @Route("/suiviNotificationMangue", name="suivi_notification_mangue")
     * @Method("GET")
     */

    public function suiviNotificationAction()
    {
        return $this->render('dpv/suiviNotificationMangue.html.twig');
    }
    /**
     *
     * @Route("/ficheExportateur", name="fiche_exportateur")
     * @Method("GET")
     */
    public function ficheExportateurAction(Request $request)
    {

        /**
         * $em = $this->getDoctrine()->getManager();
         * $demandeinspection = $em->getRepository('TpaBundle:DemandeInspection')->findBy(array('id'=>$demandeinspection->getId()));
         */
        return $this->render('dpv/ficheExportateur.html.twig');
    }

    /**
     *
     * @Route("/ficheExportateur/{idCond}", name="fiche_exportateur_bis")
     * @Method("GET")
     */
    public function ficheExportateurActionById(Request $request, $idCond)
    {
        var_dump('cette fiche n\'est pas encore disponible');
        //exit();

        /**
        $em = $this->getDoctrine()->getManager();
        $demandeinspection = $em->getRepository('TpaBundle:DemandeInspection')->findBy(array('id'=>$demandeinspection->getId()));
         */
        return $this->render('dpv/ficheExportateur.html.twig');
    }
    /**
     *
     * @Route("/ficheEvaluationExportateur", name="fiche_evaluation_exportateur")
     * @Method("GET")
     */
    public function ficheEvaluationExportateurAction()
    {
        return $this->render('dpv/ficheEvaluationMangue.html.twig');
    }




    /**
     * @Route("/detailOperateur", name="detailop")
     */



    public function showFicheOperateurAction(FicheOperateur $ficheOper)
    {
        $deleteForm = $this->createDeleteForm($ficheOper);
        $em = $this->getDoctrine()->getManager();

        $fiche = $em->getRepository('FicheOperateur')->findBy(array('fiche_operateur' => $ficheOper->getId()));
        $producteur = $em->getRepository('TpaBundle:Producteur')->findBy(array('producteur'=>$ficheOper->getProducteur()));
        return $this->render('demande/show.html.twig', array(
            'fiche' => $ficheOper,
            'delete_form' => $deleteForm->createView(),
            'producteur'=>$producteur
        ));
    }

    /**
     * @Route("/wordFichier", name="word")
     */
    public function wordAction(Request $request)
    {
        // Create a new Word document
        $phpWord = new PhpWord();

        /* Note: any element you append to a document must reside inside of a Section. */

        // Adding an empty Section to the document...
        $section = $phpWord->addSection();
        // Adding Text element to the Section having font styled by default...
        $section->addText(
            '"Learn from yesterday, live for today, hope for tomorrow. '
            . 'The important thing is not to stop questioning." '
            . '(Albert Einstein)'
        );

        // Saving the document as OOXML file...
        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');

        // Create a temporal file in the system
        $fileName = 'hello_world_download_file.docx';
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Write in the temporal filepath
        $objWriter->save($temp_file);

        // Send the temporal file as response (as an attachment)
        $response = new BinaryFileResponse($temp_file);
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $fileName
        );

        return $response;
    }

}
