<?php

namespace TpaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

class StatistiqueController extends Controller
{

    /**
     * @Route("/statistique", name="statistique")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
//liste des produit exportés par varietes
        $statExpMangue = count($em->getRepository('TpaBundle:FicheExportation')->findBy(array('variete'=>'Mangue')));
        $statExpMais = count($em->getRepository('TpaBundle:FicheExportation')->findBy(array('variete'=>'Mais doux')));
        $statExpTomate = count($em->getRepository('TpaBundle:FicheExportation')->findBy(array('variete'=>'Tomate')));
        $statExpHaricot = count($em->getRepository('TpaBundle:FicheExportation')->findBy(array('variete'=>'Haricot')));
        //liste total des produit par varietes
        $statMangue = count($em->getRepository('NosFormulairesBundle:Campagne')->findBy(array('variete'=>'Mangue')));
        $statMais = count($em->getRepository('NosFormulairesBundle:Campagne')->findBy(array('variete'=>'Mais doux')));
        $statTomate = count($em->getRepository('NosFormulairesBundle:Campagne')->findBy(array('variete'=>'Tomate')));
        $statHaricot = count($em->getRepository('NosFormulairesBundle:Campagne')->findBy(array('variete'=>'Haricot')));

        //demande count
        $demandeEnCours = count($em->getRepository('TpaBundle:Demande')->findBy(array('statut'=>0)));
        $demandeValide = count($em->getRepository('TpaBundle:Demande')->findBy(array('statut'=>1)));
        $demandeRejeter = count($em->getRepository('TpaBundle:Demande')->findBy(array('statut'=>2)));
        $demandeAll = count($em->getRepository('TpaBundle:Demande')->findAll());

        //produits
        $exportationsAll = count($em->getRepository('TpaBundle:FicheExportation')->findAll());
        $exportationEnCours = count($em->getRepository('TpaBundle:FicheExportation')->findBy(array('statut'=>0)));

        $inspectionAll = count($em->getRepository('TpaBundle:DemandeInspection')->findAll());
        $inspectionEnCours = count($em->getRepository('TpaBundle:DemandeInspection')->findBy(array('statuts'=>0)));



        return $this->render('TpaBundle:statistique:statistique.html.twig', array(
            'statExpMangue' => $statExpMangue,
            'statExpMais' => $statExpMais,
            'statExpTomate' => $statExpTomate,
            'statExpHaricot' => $statExpHaricot,
            'statMangue'=>$statMangue,
            'statMais'=>$statMais,
            'statTomate'=>$statTomate,
            'statHaricot'=>$statHaricot,

            'demandeEnCours'=>$demandeEnCours,
            'demandeValide'=>$demandeValide,
            'demandeRejeter'=>$demandeRejeter,
            'demandeAll'=>$demandeAll,

            'exportationsAll'=>$exportationsAll,
            'exportationEnCours'=>$exportationEnCours,
            'inspectionAll'=>$inspectionAll,
            'inspectionEnCours'=>$inspectionEnCours,
        ));
    }
    /**
     * @Route("/statistique/demande", name="statistique_demande")
     */
    public function demandeAction()
    {
          $em = $this->getDoctrine()->getManager();
//liste des produit exportés par varietes
        $statExpMangue = $em->getRepository('TpaBundle:FicheExportation')->findBy(array('variete'=>'Mangue'));
         $statExpMais = $em->getRepository('TpaBundle:FicheExportation')->findBy(array('variete'=>'Mais doux'));
         $statExpTomate = $em->getRepository('TpaBundle:FicheExportation')->findBy(array('variete'=>'Tomate'));
         $statExpHaricot = $em->getRepository('TpaBundle:FicheExportation')->findBy(array('variete'=>'Haricot'));
         //liste total des produit par varietes
        $statMangue = $em->getRepository('NosFormulairesBundle:Campagne')->findBy(array('variete'=>'Mangue'));
        $statMais = $em->getRepository('NosFormulairesBundle:Campagne')->findBy(array('variete'=>'Mais doux'));
        $statTomate = $em->getRepository('NosFormulairesBundle:Campagne')->findBy(array('variete'=>'Tomate'));
        $statHaricot = $em->getRepository('NosFormulairesBundle:Campagne')->findBy(array('variete'=>'Haricot'));
        return $this->render('TpaBundle:statistique:statistique_demande.html.twig', array(
            'statExpMangue' => $statExpMangue,
            'statExpMais' => $statExpMais,
            'statExpTomate' => $statExpTomate,
            'statExpHaricot' => $statExpHaricot,
            'statMangue'=>$statMangue,
            'statMais'=>$statMais,
            'statTomate'=>$statTomate,
            'statHaricot'=>$statHaricot,
        ));
    }

}
