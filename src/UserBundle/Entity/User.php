<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $nom;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $prenom;


    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $producteur_id;



    public function __construct()
    {
        parent::__construct();
        // $this->roles = array('ROLE_GERANT');
        // your own logic
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProducteurId()
    {
        return $this->producteur_id;
    }

    /**
     * @param mixed $producteur_id
     */
    public function setProducteurId($producteur_id)
    {
        $this->producteur_id = $producteur_id;
        return $this;
    }


    /**
     * @return mixed
     */
    public function getMaxRole()

    {
        /*
         *
                        'ROLE_PRODUCTEUR' => 'ROLE_PRODUCTEUR',
                        'ROLE_AGENT' => 'ROLE_AGENT',
                        'ROLE_CONTROLEUR' => 'ROLE_CONTROLEUR',
                        'ROLE_ADMIN' => 'ROLE_ADMIN',
         */
        $rolesSortedByImportance = ['ROLE_SUPER_ADMIN', 'ROLE_ADMIN', 'ROLE_CONTROLEUR', 'ROLE_AGENT', 'ROLE_PRODUCTEUR'];
        foreach ($rolesSortedByImportance as $role) {
            if (in_array($role, $this->roles)) {
                return substr($role, 5);
            }
        }

        return 'ROLE_USER'; // Unknown role?
    }




}
