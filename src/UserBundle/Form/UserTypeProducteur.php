<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class UserTypeProducteur extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('roles', ChoiceType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'style' => 'margin:5px 0;'

                ),
                'choices' =>
                    array
                    (
                        'ROLE_PRODUCTEUR' => 'ROLE_PRODUCTEUR',
//                         'ROLE_PRODUCTEUR' => 'ROLE_PRODUCTEUR',
//                        'ROLE_AGENT' => 'ROLE_AGENT',
//                        'ROLE_CONTROLEUR' => 'ROLE_CONTROLEUR',
//                        'ROLE_ADMIN' => 'ROLE_ADMIN',
//                        'ROLE_ADMIN' => 'ROLE_ADMIN',
//                        'ROLE_CONTROLEUR' => 'ROLE_CONTROLEUR',
//                        'ROLE_AGENT' => 'ROLE_AGENT',
                        //'ROLE_EXPORTATEUR' => 'ROLE_EXPORTATEUR',
                        //'ROLE_CLIENT' => 'ROLE_CLIENT',
                        //'ROLE_DPV' => 'ROLE_DPV',
                        //'ROLE_INSPECTEUR'=>'ROLE_INSPECTEUR',
                        //'ROLE_USER' => 'ROLE_USER',
                    ),
                'multiple' => true,
                'required' => true,


            )


        );//end roles


        //$builder->remove('plainPassword');
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
// Or for Symfony < 2.8
// return 'fos_user_registration';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

// For Symfony 2.x

    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }

}