<?php
namespace UserBundle\Form;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('nom', null, [
                'required' => false,
                'empty_data' => 'non renseigné',
            ])
            ->add('prenom', null, [
                'required' => false,
                'empty_data' => 'non renseigné',

            ])
            ->add('roles', ChoiceType::class, array(
                'attr' => array(
                    'class' => 'form-control',
                    'style' => 'margin:5px 0;'),
                'choices' =>
                    array
                    (
                        'ROLE_PRODUCTEUR' => 'ROLE_PRODUCTEUR',
                        'ROLE_AGENT' => 'ROLE_AGENT',
                        'ROLE_CONTROLEUR' => 'ROLE_CONTROLEUR',
                        'ROLE_ADMIN' => 'ROLE_ADMIN',

                        //'ROLE_USER' => 'ROLE_USER',
                        //'ROLE_INSPECTEUR'=>'ROLE_INSPECTEUR',
                        //'ROLE_DPV' => 'ROLE_DPV',
                        //'ROLE_CLIENT' => 'ROLE_CLIENT',
                        'ROLE_EXPORTATEUR' => 'ROLE_EXPORTATEUR',
                    ),
                'multiple' => true,
                'required' => true,
            )


            )//end roles


        ;


        //$builder->remove('plainPassword');
    }

    public function getParent()
    {
        return 'FOS\UserBundle\Form\Type\RegistrationFormType';
// Or for Symfony < 2.8
// return 'fos_user_registration';
    }
    public function getBlockPrefix()
    {
        return 'app_user_registration';
    }
// For Symfony 2.x
    public function getName()
    {
        return $this->getBlockPrefix();
    }

}